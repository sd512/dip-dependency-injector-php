<?php


namespace dip;


use RuntimeException;

class InjectionException extends RuntimeException
{

    /**
     * InjectionException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}