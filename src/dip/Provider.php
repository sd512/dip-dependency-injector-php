<?php

namespace dip;

/**
 * Interface Provider
 * @package dip
 *
 * Implement this interface when a dependency must be provided by dip but it cannot be automatically generated.
 * Instances of Provider should be registered with your Dip instance using Dip::addProvider
 */
interface Provider {

    /**
     * @return string Fully qualified class name for the type of object this Provider will produce
     */
    function forClass(): string;

    /**
     * @return mixed An instance of the class type specified in Provider::forClass()
     */
    function provide();
}