<?php

namespace dip;

/**
 * Class ValueProvider
 * @package dip
 *
 * A convenience class for making a simple provider which just returns one value.
 */
class ValueProvider implements Provider {

    private $class;
    private $value;

    /**
     * SimpleProvider constructor.
     * @param $class string Fully qualified type of the dependency provided by this class.
     * @param $value mixed The value that will be returned when a dependency of type $class is requested
     */
    public function __construct($class, $value)
    {
        $this->class = $class;
        $this->value = $value;
    }

    function forClass(): string
    {
        return $this->class;
    }

    function provide()
    {
        return $this->value;
    }


}
