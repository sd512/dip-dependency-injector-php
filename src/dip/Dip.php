<?php

namespace dip;

use \ReflectionClass;
use ReflectionException;

class Dip
{
    private $providers = [];

    /**
     * Inject into the $target object all fields annotated with {@}inject. Fields requesting injection must be
     * accompanied by a var annotation to specify the value type.
     *
     * @param $target mixed The object to be injected.
     * @throws InjectionException if a requested injection cannot be satisfied.
     */
    public function inject($target)
    {
        try {
            $reflected = new ReflectionClass($target);
        } catch (ReflectionException $re){
            throw new InjectionException("Unable to determine type of object requesting injection");
        }

        foreach ($reflected->getProperties() as $property) {

            $docComment = $property->getDocComment();

            $type = $this->getRequestedType($docComment, $reflected->getNamespaceName());

            if ($type) {

                if($property->getValue($target) !== NULL ){
                    trigger_error(
                        "Dip: Overwriting existing value \${$property->getName()} with injected value",
                        E_USER_WARNING);
                }

                $property->setValue(
                    $target,
                    $this->provide($type)
                );
            }
        }
    }

    /**
     * Parse a PHPDoc comment to determine if an inject and var annotation are present.
     *
     * @param $phpdocComment string A comment possibly containing {@}inject and {@}var annotations
     * @param $namespace string The namespace of the object from which the comment was extracted
     * @return string|null A string specifying the type indicated in the {@}var annotation. Null is returned if either
     * {@}inject or {@}var are not present.
     */
    private function getRequestedType($phpdocComment, $namespace)
    {
        //fixme: consider use statements

        // if @inject annotation is present
        if (preg_match('/^(?:\\/\*|[\*\s])*@inject\s*/m', $phpdocComment)) {

            // if @var annotation is defined with type
            if (preg_match_all('/^(?:\\/\*|\s|\*)*(?:@[^\s]+\s*)*\s*@var[ \t]+([^\s*]+)/m', $phpdocComment, $matches)) {

                $type = $matches[1][0];
                if ($type{0} !== "\\" && !empty($namespace)) {
                    $type = "\\" . $namespace . "\\" . $type;
                }
                return $type;
            }
        }
        return null;

    }

    /**
     * Create and return an instance of a class based on a type string.
     *
     * @param $type string The class type to be created.
     * @return mixed An instance of $type class
     * @throws InjectionException if Provider implementation does not provide a value
     */
    public function provide($type)
    {
        if (isset($this->providers[$type])) {
            $provided = $this->providers[$type]->provide();
            if($provided == null){
                throw new InjectionException("Provider for type $type returned null");
            }
        } else {
            $provided = $this->provideFromScratch($type);
        }
        $this->inject($provided);
        return $provided;
    }

    /**
     * Create and return an instance of a class based on a type string. Determine dependencies needed through reflection.
     *
     * @param $type string The class type to be created
     * @return mixed an instance of $type class
     * @throws InjectionException if unable to determine the type of the object being requested.
     */
    private function provideFromScratch($type)
    {
        //todo cyclic dependency detection

        try {
            $class = new ReflectionClass($type);
        } catch (ReflectionException $re){
            throw new InjectionException("Unable to provide dependency of unknown type ". $type);
        }
        $ctor = $class->getConstructor();

        $params = [];
        $paramInstances = [];

        if ($ctor) {
            $params = $ctor->getParameters();
        }

        foreach ($params as $param) {
            $class = $param->getClass();
            $class_name = $class ? $class->getName(): null;
            $paramInstances[] = $this->provide($class_name);
        }

        return new $type(... $paramInstances);
    }

    /**
     * Register a provider to be used as needed to create requested dependencies
     *
     * @param Provider $provider The provider to be added
     */
    public function addProvider(Provider $provider)
    {
        $this->inject($provider);

        $this->providers[$provider->forClass()] = $provider;

    }
}
