<?php

namespace dip\test
{
    use dip\Provider;
    use FooGlobal;
    use other\FooFromOtherNamespace;

    class Foo
    {
    }

    class FooEmptyCtor
    {
        public function __construct()
        {
        }
    }

    class FooResolvableCtorArgs
    {
        public $foo;
        public $fooc;

        public function __construct(Foo $foo, FooEmptyCtor $fooc)
        {
            $this->foo = $foo;
            $this->fooc = $fooc;
        }
    }

    class FooUnqualifiedArg
    {
        public $bar;

        public function __construct($unqualified)
        {
            $this->bar = $unqualified;
        }
    }

    class FooUnqualifiedArgWithInjectField
    {
        /**
         * @inject
         * @var Foo
         */
        public $foo;

        public $bar;

        public function __construct($unqualified)
        {
            $this->bar = $unqualified;
        }
    }

    class FooUnresolvableQualifiedArgWithInjectField
    {
        public function __construct(string $string)
        {
        }
    }

    class FooArgFromAnotherNamespace
    {
        public $bar;

        public function __construct(FooFromOtherNamespace $ffons)
        {
            $this->bar = $ffons;
        }
    }

    class FooProviderWithInject implements Provider
    {

        /**
         * @inject
         * @var Foo
         */
        public $foo;

        function forClass(): string
        {
            return "";
        }

        function provide()
        {
            return [];
        }
    }

    class FooWithInjectableField
    {

        /**
         * @inject
         * @var Foo
         */
        public $foo;
    }

    class FooWithInjectableFieldFromOtherNamespace
    {

        /**
         * @inject
         * @var FooFromOtherNamespace
         */
        public $fooo;
    }

    class FooHasInjectFromOtherNamespace
    {
        /**
         * @inject
         * @var FooFromOtherNamespace
         */
        public $foo;
    }

    class FooWithInjectableFieldWithFullNamespace
    {

        /**
         * @inject
         * @var \other\FooFromOtherNamespace
         */
        public $fooo;
    }
}

namespace other
{
    class FooFromOtherNamespace
    {
    }
}

namespace
{
    class FooGlobal{}
}