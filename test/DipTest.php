<?php

declare(strict_types=1);

namespace dip\test;

require_once 'DipTestClasses.php';

use dip\Dip;
use dip\InjectionException;
use dip\Provider;
use FooGlobal;
use other\FooFromOtherNamespace;
use PHPUnit\Framework\TestCase;
//use PHPUnit\Framework\Warning;
use PHPUnit\Framework\Error\Warning;


final class DipTest extends TestCase
{

    /**
     * @var Dip
     */
    private $dip;

    /**
     * @before
     */
    public function reset()
    {
        $this->dip = new Dip();
    }

    public function testCanProvidePlainObject(): void
    {
        self::assertInstanceOf(
            Foo::class,
            $this->dip->provide(Foo::class)
        );
    }

    public function testCanProvideObjectWithDefaultConstructor(): void
    {
        self::assertInstanceOf(
            FooEmptyCtor::class,
            $this->dip->provide(FooEmptyCtor::class)
        );
    }

    public function testCanProvideObjectWithInjectableDependencies(): void
    {
        $provided = $this->dip->provide(FooResolvableCtorArgs::class);

        $this->assertInstanceOf(
            FooResolvableCtorArgs::class,
            $provided
        );

        self::assertInstanceOf(
            Foo::class,
            $provided->foo
        );

        self::assertInstanceOf(
            FooEmptyCtor::class,
            $provided->fooc
        );
    }

    public function testCannotProvideObjectWithUnspeifiedParamTypes(): void
    {
        $this->expectException(InjectionException::class);

        $this->dip->provide(FooUnqualifiedArg::class);
    }

    public function testCannotProvideObjectWithUnresolvableParameterType(): void
        {
            $this->expectException(InjectionException::class);

            $this->dip->provide(FooUnresolvableQualifiedArgWithInjectField::class);
        }

    public function testCanProvideObjectWithUseStatementDependency():void {
        $provided = $this->dip->provide(FooArgFromAnotherNamespace::class);

        $this->assertInstanceOf(
            FooArgFromAnotherNamespace::class,
            $provided
        );

        self::assertInstanceOf(
            FooFromOtherNamespace::class,
            $provided->bar
        );
    }

    public function testCanProvideDependencyFromProvider():void {
        $obj = [];
        $provider_provided = new FooUnqualifiedArg($obj);

        $mock = $this->createMock(Provider::class);
        $mock->method('forClass')->willReturn(FooUnqualifiedArg::class);
        $mock->method('provide')->willReturn($provider_provided);

        $this->dip->addProvider($mock);

        $provided = $this->dip->provide(FooUnqualifiedArg::class);

        $this->assertSame($provided, $provider_provided);
        $this->assertSame($obj, $provided->bar);

    }

    public function testCanProvideInjectedDependencyFromProvider():void {

        $provider_provided = new FooUnqualifiedArgWithInjectField([]);

        $mock = $this->createMock(Provider::class);
        $mock->method('forClass')->willReturn(FooUnqualifiedArgWithInjectField::class);
        $mock->method('provide')->willReturn($provider_provided);

        $this->dip->addProvider($mock);

        $provided = $this->dip->provide(FooUnqualifiedArgWithInjectField::class);

        $this->assertInstanceOf(
            Foo::class,
            $provided->foo
        );
    }

    public function testCanInjectFromOtherNamespace():void{
        $obj = new FooWithInjectableFieldFromOtherNamespace();
        $this->dip->inject($obj);

        self::assertInstanceOf(
            FooFromOtherNamespace::class,
            $obj->fooo
        );
    }

    public function testCanInjectFullyQualified():void{
        $obj = new FooWithInjectableFieldWithFullNamespace();
        $this->dip->inject($obj);

        self::assertInstanceOf(
            FooFromOtherNamespace::class,
            $obj->fooo
        );
    }

    public function testProviderGetsInjected():void {
        $provider = new FooProviderWithInject();

        $this->dip->addProvider($provider);

        self::assertInstanceOf(
            Foo::class,
            $provider->foo
        );
    }

    public function testInjectingIntoFieldWithValueProducesWarning():void {

        $this->expectException(Warning::class);

        $presetInjectedField = new FooWithInjectableField();
        $presetInjectedField->foo = new Foo();

        $this->dip->inject($presetInjectedField);
    }

}
